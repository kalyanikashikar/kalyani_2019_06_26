var selectedCanvasID;
var canvasArray = [];
function generateRandomCanvas(){
	var noOfCanvases, i, canvas_node, canvasId, canvas, selectList; 
	noOfCanvases= Math.round(4 * Math.random());
	if (noOfCanvases == 1) { 
		noOfCanvases = noOfCanvases+1;
	}
	selectList = $("#canvasSelector");
	selectList.find("option:gt(0)").remove();
	$("canvas").css("display", "none");
	for(i = 1; i <= noOfCanvases ; i++){
		canvas = new fabric.Canvas('canvas-node' + i);
		canvas.id = 'canvas' + i;
		canvasId = i;
		canvas.setWidth(300);
		canvas.setHeight(300);
		$("canvas").css("display", "block");
		$(".canvasWrapper").append(canvas);
		$("#canvasSelector").append("<option>" + canvasId + "</option>");
		canvasArray.push(canvas);
	}
}
function enableInsert(){
	selectedCanvasID = $("#canvasSelector").val();
	if($("#canvasSelector").val() != 'Canvas#'){
		$('#insertCanvas').attr("disabled", false)
	} else{
		$('#insertCanvas').attr("disabled", true)
	}
}

function insertToCanvas(){
	$.ajax({
		url: "https://jsonplaceholder.typicode.com/photos", 
		success: function(result)
		{
			insertToCanvasSuccess(result);
		}
	});
}

function insertToCanvasSuccess(result){
	var resultLength, lastElement, firstElement, radomElement,canvasSelectedId, resultData ;
	if(result.length){
		resultLength = result.length;
		lastElement = result.slice(resultLength-2,resultLength+1);
		firstElement = result.slice(0,2);
		radomElement = result[Math.round((resultLength-2) * Math.random())];
		canvasSelectedId = $("#canvasSelector").val();
		resultData = firstElement.concat(lastElement, radomElement);
		imageLoad(resultData);
	}
}

function imageLoad(resultData){
	var selectedCanvas;
	canvasArray.some(function(item, index) {
		if(item.id === 'canvas'+selectedCanvasID) {
			selectedCanvas = item;
		}
	})
	for(var i=0; i <=resultData.length; i++){
		if(resultData[i]) {
			if(resultData[i].albumId >=100){
				if (resultData[i].url){
					var url = new fabric.Text(resultData[i].url, {
						fontSize: 20,
						isDragging:true,
						left: 30, 
						top: 30 * i, 
						hasControls: true,
						lockUniScaling: true
					});
					selectedCanvas.add(url);
				}
			} else {
				if(resultData[i].id % 2 === 0 ){
					if (resultData[i].title){
						var text = new fabric.Text(resultData[i].title, {
							fontSize: 20,
							isDragging:true,
							left: 30, 
							top: 30 *i ,
							hasControls: true,
							lockUniScaling: true
						});
						selectedCanvas.add(text);
					}
				} else {
					if (resultData[i].thumbnailUrl){
						var loadImage = fabric.Image.fromURL(resultData[i].thumbnailUrl, function(img) {
							isImageLoaded = true;
							loadImage = img.set({
								selectable: true,
								isDragging:true,
								left: 30, 
								top: 30*i 
							}).scale(0.5);
							selectedCanvas.add(loadImage);
							
						});
					}
				}
			}
		}
	}
	selectedCanvas.on('mouse:down', function() {
		if(this.getActiveObject()) {
			activeObject  = $.extend({}, this.getActiveObject());
			initialCanvas = this.lowerCanvasEl.id;
		}
	});
	selectedCanvas.on('mouse:up', function() {
		if(this.getActiveObject()) {
			activeObject  = $.extend({}, this.getActiveObject());
			initialCanvas = this.lowerCanvasEl.id;
		}
	});
	selectedCanvas.on('mouse:left', function() {
		if(this.getActiveObject()) {
			activeObject  = $.extend({}, this.getActiveObject());
			initialCanvas = this.lowerCanvasEl.id;
		}
	});
	selectedCanvas.on('mouse:right', function() {
		if(this.getActiveObject()) {
			activeObject  = $.extend({}, this.getActiveObject());
			initialCanvas = this.lowerCanvasEl.id;
		}
	});

	$(document).on('mouseup', function(evt) {
		if(evt.target.localName === 'canvas' && initialCanvas) {
			canvasId = $(evt.target).siblings().attr('id');
			if(canvasId !== initialCanvas) {
				var index = parseInt(canvasId.split("node")[1])
				canvasArray[index-1].add(activeObject);
				canvasArray[index-1].renderAll();
			}
		}
		initialCanvas = '';
		activeObject  = {};                 
	}); 
}